# Latvian National Armed Forces

## Contributing

### Coding Guidelines

The coding guidelines are the same as it is for ACE3.

### Config file technicalities

- Config file entries for items must be divided into the amount of variations (e.g. LATPAT, MultiLATPAT).
- Config file entries must be ordered alphabetically by their textures in their `data` folders. If not available then alphabetically by their classnames (this applies to the `stringtable.xml` too).
- Other variants of items must inherit the primary variation (e.g. LATPAT).
- The separation of two variants must be indicated by a 3 line block level comment.

### Component naming

- Components must be named plurally if they contain clothing/equipment. As there are multiples of them (even though only one is currently present) it must be named plurally.

class CfgVehicles {
    class B_AssaultPack_blk;
    class B_Carryall_khk;
    class B_FieldPack_blk;
    class B_Kitbag_cbr;
    class B_Bergen_mcamo_F;

    /**
     * LATPAT
     */
    class GVAR(AssaultPack_LATPAT) : B_AssaultPack_blk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(AssaultPack_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_compact_latpat_co.paa)};
    };

    class GVAR(Carryall_LATPAT) : B_Carryall_khk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Carryall_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_tortila_latpat_co.paa)};
    };

    class GVAR(FieldPack_LATPAT) : B_FieldPack_blk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(FieldPack_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_gorod_latpat_co.paa)};
    };

    class GVAR(Kitbag_LATPAT) : B_Kitbag_cbr {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Kitbag_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_fast_latpat_co.paa)};
    };

    class GVAR(Bergen_LATPAT) : B_Bergen_mcamo_F {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bergen_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\bergen_latpat_co.paa)};
    };

    /**
     * MultiLATPAT
     */
    class GVAR(AssaultPack_Multi_LATPAT) : GVAR(AssaultPack_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(AssaultPack_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_compact_multi_latpat_co.paa)};
    };

    class GVAR(Carryall_Multi_LATPAT) : GVAR(Carryall_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Carryall_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_tortila_multi_latpat_co.paa)};
    };

    class GVAR(FieldPack_Multi_LATPAT) : GVAR(FieldPack_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(FieldPack_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_gorod_multi_latpat_co.paa)};
    };

    class GVAR(Kitbag_Multi_LATPAT) : GVAR(Kitbag_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Kitbag_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\backpack_fast_multi_latpat_co.paa)};
    };

    class GVAR(Bergen_Multi_LATPAT) : GVAR(Bergen_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bergen_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\bergen_multi_latpat_co.paa)};
    };
};

class CfgGlasses {
    class G_Bandanna_khk;
    class G_Bandanna_shades;
    class G_Bandanna_sport;
    class G_Bandanna_aviator;

    class G_Balaclava_TI_blk_F;
    class G_Balaclava_TI_G_blk_F;

    /**
     * LATPAT
     */
    class GVAR(Bandanna_LATPAT) : G_Bandanna_khk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_latpat_co.paa)};
    };

    class GVAR(Bandanna_shades_LATPAT) : G_Bandanna_shades {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_shades_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_latpat_co.paa), "\a3\characters_f\heads\glasses\data\joeyx_black_ca.paa"};
    };

    class GVAR(Bandanna_sport_LATPAT) : G_Bandanna_sport {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_sport_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_latpat_co.paa), "\a3\characters_f\heads\glasses\data\sunglasses_sport_4_ca.paa"};
    };

    class GVAR(Bandanna_aviator_LATPAT) : G_Bandanna_aviator {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_aviator_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_latpat_co.paa), "\a3\characters_f\heads\glasses\data\glass_ca.paa"};
    };

    class GVAR(Balaclava_TI_LATPAT) : G_Balaclava_TI_blk_F {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Balaclava_TI_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\G_Balaclava_TI_blk_F_latpat_co.paa)};
    };

    class GVAR(Balaclava_TI_G_LATPAT) : G_Balaclava_TI_G_blk_F {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Balaclava_TI_G_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\G_Balaclava_TI_blk_F_latpat_co.paa), "\A3\Characters_F\Heads\Glasses\data\g_combat_ca.paa"};
    };

    /**
     * MultiLATPAT
     */
    class GVAR(Bandanna_Multi_LATPAT) : GVAR(Bandanna_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_multi_latpat_co.paa)};
    };

    class GVAR(Bandanna_shades_Multi_LATPAT) : GVAR(Bandanna_shades_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_shades_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_multi_latpat_co.paa), "\a3\characters_f\heads\glasses\data\joeyx_black_ca.paa"};
    };

    class GVAR(Bandanna_sport_Multi_LATPAT) : GVAR(Bandanna_sport_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_sport_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_multi_latpat_co.paa), "\a3\characters_f\heads\glasses\data\sunglasses_sport_4_ca.paa"};
    };

    class GVAR(Bandanna_aviator_Multi_LATPAT) : GVAR(Bandanna_aviator_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_aviator_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_bandMask_multi_latpat_co.paa), "\a3\characters_f\heads\glasses\data\glass_ca.paa"};
    };

    class GVAR(Balaclava_TI_Multi_LATPAT) : GVAR(Balaclava_TI_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Balaclava_TI_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\G_Balaclava_TI_blk_F_multi_latpat_co.paa)};
    };

    class GVAR(Balaclava_TI_G_Multi_LATPAT) : GVAR(Balaclava_TI_G_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Balaclava_TI_G_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\G_Balaclava_TI_blk_F_multi_latpat_co.paa), "\A3\Characters_F\Heads\Glasses\data\g_combat_ca.paa"};
    };
};

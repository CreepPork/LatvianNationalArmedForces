#define COMPONENT facewears
#define COMPONENT_BEAUTIFIED Facewears
#include "\x\lnaf\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE
// #define ENABLE_PERFORMANCE_COUNTERS

#ifdef DEBUG_ENABLED_FACEWEARS
    #define DEBUG_MODE_FULL
#endif

#ifdef DEBUG_SETTINGS_FACEWEARS
    #define DEBUG_SETTINGS DEBUG_SETTINGS_FACEWEARS
#endif

#include "\x\lnaf\addons\main\script_macros.hpp"

class CfgVehicles {
    class FlagCarrier;

    class GVAR(flagLatvia) : FlagCarrier {
        author = ECSTRING(main,Author);
        scope = 2;
        scopeCurator = 2;
        class EventHandlers {
            init = QUOTE((_this select 0) setFlagTexture QPATHTOF(data\latvia_ca.paa));
        };
    };

    class GVAR(flagEstonia) : FlagCarrier {
        author = ECSTRING(main,Author);
        scope = 2;
        scopeCurator = 2;
        class EventHandlers {
            init = QUOTE((_this select 0) setFlagTexture QPATHTOF(data\estonia_ca.paa));
        };
    };

    class GVAR(flagLithuania) : FlagCarrier {
        author = ECSTRING(main,Author);
        scope = 2;
        scopeCurator = 2;
        class EventHandlers {
            init = QUOTE((_this select 0) setFlagTexture QPATHTOF(data\lithuania_ca.paa));
        };
    };
};

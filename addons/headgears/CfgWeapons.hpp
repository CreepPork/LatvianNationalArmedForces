class CfgWeapons {
    class H_HelmetIA;
    class H_MilCap_mcamo;
    class H_Booniehat_dgtl;
    class H_HelmetB;
    class H_HelmetB_light;
    class H_HelmetSpecB;
    class H_Bandanna_sand;
    class H_Cap_tan;

    /**
     * LATPAT
     */
    class GVAR(HelmetIA_LATPAT) : H_HelmetIA {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetIA_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_helmet_latpat_co.paa)};
    };

    class GVAR(MilCap_LATPAT) : H_MilCap_mcamo {
        author = ECSTRING(main,Author);
        displayName = CSTRING(MilCap_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\cappatrol_latpat_co.paa)};
    };

    class GVAR(Booniehat_LATPAT) : H_Booniehat_dgtl {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Booniehat_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\booniehat_latpat_co.paa)};
    };

    class GVAR(HelmetB_LATPAT) : H_HelmetB {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetB_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_latpat_co.paa)};
    };

    class GVAR(HelmetSpecB_LATPAT) : H_HelmetSpecB {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetSpecB_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_latpat_co.paa)};
    };

    class GVAR(HelmetB_light_LATPAT) : H_HelmetB_light {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetB_light_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_latpat_co.paa)};
    };

    class GVAR(Bandanna_LATPAT) : H_Bandanna_sand {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\h_bandana_latpat_co.paa)};
    };

    class GVAR(Cap_LATPAT) : H_Cap_tan {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Cap_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\capb_latpat_co.paa)};
    };

    /**
     * MultiLATPAT
     */
     class GVAR(HelmetIA_Multi_LATPAT) : GVAR(HelmetIA_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetIA_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\headgear_helmet_multi_latpat_co.paa)};
    };

    class GVAR(MilCap_Multi_LATPAT) : GVAR(MilCap_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(MilCap_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\cappatrol_multi_latpat_co.paa)};
    };

    class GVAR(Booniehat_Multi_LATPAT) : GVAR(Booniehat_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Booniehat_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\booniehat_multi_latpat_co.paa)};
    };

    class GVAR(HelmetB_Multi_LATPAT) : GVAR(HelmetB_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetB_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_multi_latpat_co.paa)};
    };

    class GVAR(HelmetSpecB_Multi_LATPAT) : GVAR(HelmetSpecB_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetSpecB_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_multi_latpat_co.paa)};
    };

    class GVAR(HelmetB_light_Multi_LATPAT) : GVAR(HelmetB_light_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetB_light_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_multi_latpat_co.paa)};
    };

    class GVAR(Bandanna_LATPAT) : GVAR(Bandanna_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Bandanna_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\h_bandana_latpat_co.paa)};
    };

    class GVAR(Cap_Multi_LATPAT) : GVAR(Cap_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Cap_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\capb_multi_latpat_co.paa)};
    };

    class GVAR(HelmetB_E23_Multi_LATPAT) : GVAR(HelmetB_Multi_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetB_E23_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_multi_latpat_e23_co.paa)};
    };

    class GVAR(HelmetB_E24_Multi_LATPAT) : GVAR(HelmetB_Multi_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetB_E24_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_multi_latpat_e24_co.paa)};
    };

    class GVAR(HelmetB_E25_Multi_LATPAT) : GVAR(HelmetB_Multi_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(HelmetB_E25_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip1_multi_latpat_e25_co.paa)};
    };
};

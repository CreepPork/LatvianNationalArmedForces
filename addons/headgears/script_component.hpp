#define COMPONENT headgears
#define COMPONENT_BEAUTIFIED Headgears
#include "\x\lnaf\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE
// #define ENABLE_PERFORMANCE_COUNTERS

#ifdef DEBUG_ENABLED_HEADGEARS
    #define DEBUG_MODE_FULL
#endif

#ifdef DEBUG_SETTINGS_HEADGEARS
    #define DEBUG_SETTINGS DEBUG_SETTINGS_HEADGEARS
#endif

#include "\x\lnaf\addons\main\script_macros.hpp"

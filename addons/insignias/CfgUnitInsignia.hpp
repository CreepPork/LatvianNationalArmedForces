class CfgUnitInsignia {
    class GVAR(Latvia) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Latvia);
        texture = QPATHTOF(data\latvia_ca.paa);
    };

    class GVAR(E23) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(E23);
        texture = QPATHTOF(data\e23_ca.paa);
    };

    class GVAR(E24) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(E24);
        texture = QPATHTOF(data\e24_ca.paa);
    };

    class GVAR(E25) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(E25);
        texture = QPATHTOF(data\e25_ca.paa);
    };
};

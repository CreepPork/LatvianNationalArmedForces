#include "script_component.hpp"

class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"lnaf_common"};
        author = ECSTRING(main,Author);
        authors[] = {"CreepPork"};
        url = ECSTRING(main,URL);
        VERSION_CONFIG;
    };
};

#include "CfgUnitInsignia.hpp"

#define COMPONENT insignias
#define COMPONENT_BEAUTIFIED Insignias
#include "\x\lnaf\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE
// #define ENABLE_PERFORMANCE_COUNTERS

#ifdef DEBUG_ENABLED_INSIGNIAS
    #define DEBUG_MODE_FULL
#endif

#ifdef DEBUG_SETTINGS_INSIGNIAS
    #define DEBUG_SETTINGS DEBUG_SETTINGS_INSIGNIAS
#endif

#include "\x\lnaf\addons\main\script_macros.hpp"

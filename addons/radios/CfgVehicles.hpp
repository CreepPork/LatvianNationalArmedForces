class CfgVehicles {
    class B_RadioBag_01_mtp_F;

    class GVAR(RadioBag_01_LATPAT): B_RadioBag_01_mtp_F {
        author = ECSTRING(main,Author);
        displayName = CSTRING(RadioBag_01_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\B_RadioBag_01_latpat_co.paa)};

        tf_dialog = "rt1523g_radio_dialog";
        tf_dialogUpdate = "call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 20000;
        tf_subtype = "digital_lr";
    };

    class GVAR(RadioBag_01_Multi_LATPAT): GVAR(RadioBag_01_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(RadioBag_01_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\B_RadioBag_01_multi_latpat_co.paa)};

        tf_dialog = "rt1523g_radio_dialog";
        tf_dialogUpdate = "call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 20000;
        tf_subtype = "digital_lr";
    };
};

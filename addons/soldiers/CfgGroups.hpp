class CfgGroups {
    class WEST {
        name = "$STR_West";
        side = SIDE_WEST;
        class GVAR(Faction) {
            name = ECSTRING(main,DisplayName);
            class Infantry_LATPAT {
                name = "$STR_mptable_infantry";
                #include "groups\FireTeam_LATPAT.hpp"
            };
            class Infantry_Multi_LATPAT {
                name = CSTRING(Infantry_Multi_LATPAT);
                #include "groups\FireTeam_Multi_LATPAT.hpp"
            };
        };
    };
};

class CfgVehicles {
    class B_Soldier_F;

    class GVAR(Soldier) : B_soldier_F {
        author = ECSTRING(main,Author);
        scope = 1;
        model = "\A3\characters_f\BLUFOR\b_soldier_01.p3d";
        identityTypes[] = {"LanguageGRE_F","Head_Greek",0};
        genericNames = QGVAR(LatvianNames);
        faction = QGVAR(Faction);
        hiddenSelections[] = {"Camo", "Insignia"};
        respawnWeapons[] = {"Throw", "Put"};
        respawnMagazines[] = {""};
        respawnLinkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"};
    };

    /**
     * LATPAT
     */
    class GVAR(TeamLeader_LATPAT) : GVAR(Soldier) {
        scope = 2;
        displayName = "$STR_b_soldier_tl_f0";
        uniformClass = QEGVAR(uniforms,CombatUniform_vest_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_vest_LATPAT);
        backpack = "";
        linkedItems[] = {QEGVAR(vests,PlateCarrierIAGL_LATPAT),QEGVAR(headgears,HelmetSpecB_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        icon = "iconManLeader";
        model = "\A3\characters_F\BLUFOR\b_soldier_03.p3d";
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_latpat_co.paa)};
    };

    class GVAR(Autorifleman_LATPAT) : GVAR(Soldier) {
        scope = 2;
        displayName = "$STR_b_soldier_ar_f0";
        uniformClass = QEGVAR(uniforms,CombatUniform_tshirt_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_tshirt_LATPAT);
        backpack = "";
        linkedItems[] = {QEGVAR(vests,PlateCarrier2_LATPAT),QEGVAR(headgears,HelmetB_light_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        icon = "iconManMG";
        model = "\A3\characters_F\BLUFOR\b_soldier_02.p3d";
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_latpat_co.paa)};
    };

    class GVAR(RiflemanAT_LATPAT) : GVAR(Soldier) {
        scope = 2;
        displayName = "$STR_b_soldier_lat_f0";
        uniformClass = QEGVAR(uniforms,CombatUniform_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_LATPAT);
        backpack = "";
        linkedItems[] = {QEGVAR(vests,PlateCarrier1_LATPAT),QEGVAR(headgears,HelmetB_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        icon = "iconManAT";
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_latpat_co.paa)};
    };

    class GVAR(Rifleman_LATPAT) : GVAR(Soldier) {
        scope = 2;
        displayName = "$STR_dn_rifleman";
        uniformClass = QEGVAR(uniforms,CombatUniform_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_LATPAT);
        backpack = "";
        linkedItems[] = {QEGVAR(vests,PlateCarrier1_LATPAT),QEGVAR(headgears,HelmetB_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_latpat_co.paa)};
    };

    class GVAR(Officer_LATPAT) : GVAR(Soldier) {
        scope = 2;
        displayName = "$STR_b_officer_f0";
        model = "\A3\Characters_F_Beta\INDEP\ia_officer.p3d";
        uniformClass = QEGVAR(uniforms,OfficerUniform_LATPAT);
        hiddenSelections[] = {"Camo1", "Camo2", "Insignia"};
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\officer_spc_latpat_co.paa), QPATHTOEF(uniforms,data\ia_soldier_01_clothing_latpat_co.paa)};
        icon = "iconManOfficer";
    };

    /**
     * MultiLATPAT
     */
    class GVAR(TeamLeader_Multi_LATPAT) : GVAR(TeamLeader_LATPAT) {
        uniformClass = QEGVAR(uniforms,CombatUniform_vest_Multi_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_vest_Multi_LATPAT);
        linkedItems[] = {QEGVAR(vests,PlateCarrierIAGL_Multi_LATPAT),QEGVAR(headgears,HelmetSpecB_Multi_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_multi_latpat_co.paa)};
        editorSubcategory = QGVAR(Personnel_Multi_LATPAT);
        model = "\A3\characters_F\BLUFOR\b_soldier_03.p3d";
    };

    class GVAR(Autorifleman_Multi_LATPAT) : GVAR(Autorifleman_LATPAT) {
        uniformClass = QEGVAR(uniforms,CombatUniform_tshirt_Multi_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_tshirt_Multi_LATPAT);
        linkedItems[] = {QEGVAR(vests,PlateCarrier2_Multi_LATPAT),QEGVAR(headgears,HelmetB_light_Multi_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_multi_latpat_co.paa)};
        editorSubcategory = QGVAR(Personnel_Multi_LATPAT);
        model = "\A3\characters_F\BLUFOR\b_soldier_02.p3d";
    };

    class GVAR(RiflemanAT_Multi_LATPAT) : GVAR(RiflemanAT_LATPAT) {
        uniformClass = QEGVAR(uniforms,CombatUniform_Multi_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_Multi_LATPAT);
        linkedItems[] = {QEGVAR(vests,PlateCarrier1_Multi_LATPAT),QEGVAR(headgears,HelmetB_Multi_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_multi_latpat_co.paa)};
        editorSubcategory = QGVAR(Personnel_Multi_LATPAT);
    };

    class GVAR(Rifleman_Multi_LATPAT) : GVAR(Rifleman_LATPAT) {
        uniformClass = QEGVAR(uniforms,CombatUniform_Multi_LATPAT);
        uniform = QEGVAR(uniforms,CombatUniform_Multi_LATPAT);
        linkedItems[] = {QEGVAR(vests,PlateCarrier1_Multi_LATPAT),QEGVAR(headgears,HelmetB_Multi_LATPAT),"ItemMap","ItemCompass","ItemWatch","ItemRadio","ItemGPS"};
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\clothing1_multi_latpat_co.paa)};
        editorSubcategory = QGVAR(Personnel_Multi_LATPAT);
    };

    class GVAR(Officer_Multi_LATPAT) : GVAR(Officer_LATPAT) {
        uniformClass = QEGVAR(uniforms,OfficerUniform_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOEF(uniforms,data\officer_spc_multi_latpat_co.paa), QPATHTOEF(uniforms,data\ia_soldier_01_clothing_multi_latpat_co.paa)};
        editorSubcategory = QGVAR(Personnel_Multi_LATPAT);
    };
};

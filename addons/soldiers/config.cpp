#include "script_component.hpp"

class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {
            // LATPAT
            QGVAR(TeamLeader_LATPAT),
            QGVAR(Autorifleman_LATPAT),
            QGVAR(RiflemanAT_LATPAT),
            QGVAR(Rifleman_LATPAT),
            QGVAR(Officer_LATPAT),
            // MultiLATPAT
            QGVAR(TeamLeader_Multi_LATPAT),
            QGVAR(Autorifleman_Multi_LATPAT),
            QGVAR(RiflemanAT_Multi_LATPAT),
            QGVAR(Rifleman_Multi_LATPAT),
            QGVAR(Officer_Multi_LATPAT)
        };
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {"lnaf_common", "lnaf_headgears", "lnaf_uniforms", "lnaf_vests"};
        author = ECSTRING(main,Author);
        authors[] = {"CreepPork"};
        url = ECSTRING(main,URL);
        VERSION_CONFIG;
    };
};

#include "CfgEditorSubcategories.hpp"
#include "CfgFactionClasses.hpp"
#include "CfgGroups.hpp"
#include "CfgVehicles.hpp"
#include "CfgWorlds.hpp"

class GVAR(FireTeam_LATPAT) {
    name = "$STR_a3_cfggroups_west_blu_f_infantry_bus_infteam0";
    faction = GVAR(Faction);
    side = SIDE_WEST;
    icon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
    class Unit0 {
        side = SIDE_WEST;
        vehicle = QGVAR(TeamLeader_LATPAT);
        rank = RANK_SERGEANT;
        position[] = {0, 0, 0};
    };
    class Unit1 {
        side = SIDE_WEST;
        vehicle = QGVAR(Autorifleman_LATPAT);
        rank = RANK_CORPORAL;
        position[] = {5, -5, 0};
    };
    class Unit2 {
        side = SIDE_WEST;
        vehicle = QGVAR(RiflemanAT_LATPAT);
        rank = RANK_PRIVATE;
        position[] = {-5, 5, 0};
    };
    class Unit3 {
        side = SIDE_WEST;
        vehicle = QGVAR(Rifleman_LATPAT);
        rank = RANK_PRIVATE;
        position[] = {10, -10, 0};
    };
};

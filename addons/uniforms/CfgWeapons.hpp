class CfgWeapons {
    class UniformItem;
    class U_B_CombatUniform_mcam;
    class U_B_CombatUniform_mcam_tshirt;
    class U_B_CombatUniform_mcam_vest;
    class U_I_OfficerUniform;

    /**
     * LATPAT
     */
    class GVAR(CombatUniform_LATPAT) : U_B_CombatUniform_mcam {
        author = ECSTRING(main,Author);
        displayName = CSTRING(CombatUniform_LATPAT);
        picture = QPATHTOF(ui\icon_u_b_combatuniform_latpat_ca.paa);
        hiddenSelectionsTextures[] = {QPATHTOF(data\clothing1_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,Rifleman_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class GVAR(CombatUniform_tshirt_LATPAT) : U_B_CombatUniform_mcam_tshirt {
        author = ECSTRING(main,Author);
        displayName = CSTRING(CombatUniform_tshirt_LATPAT);
        picture = QPATHTOF(ui\icon_u_b_combatuniform_latpat_ca.paa);
        hiddenSelectionsTextures[] = {QPATHTOF(data\clothing1_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,Autorifleman_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class GVAR(CombatUniform_vest_LATPAT) : U_B_CombatUniform_mcam_vest {
        author = ECSTRING(main,Author);
        displayName = CSTRING(CombatUniform_vest_LATPAT);
        picture = QPATHTOF(ui\icon_u_b_combatuniform_latpat_ca.paa);
        hiddenSelectionsTextures[] = {QPATHTOF(data\clothing1_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,TeamLeader_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class GVAR(OfficerUniform_LATPAT) : U_I_OfficerUniform {
        author = ECSTRING(main,Author);
        displayName = CSTRING(OfficerUniform_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\officer_spc_latpat_co.paa), QPATHTOF(data\ia_soldier_01_clothing_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,Officer_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };

    /**
     * MultiLATPAT
     */
    class GVAR(CombatUniform_Multi_LATPAT) : GVAR(CombatUniform_LATPAT) {
        displayName = CSTRING(CombatUniform_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\clothing1_multi_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,Rifleman_Multi_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class GVAR(CombatUniform_tshirt_Multi_LATPAT) : GVAR(CombatUniform_tshirt_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(CombatUniform_tshirt_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\clothing1_multi_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,Autorifleman_Multi_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class GVAR(CombatUniform_vest_Multi_LATPAT) : GVAR(CombatUniform_vest_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(CombatUniform_vest_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\clothing1_multi_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,TeamLeader_Multi_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };

    class GVAR(OfficerUniform_Multi_LATPAT) : GVAR(OfficerUniform_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(OfficerUniform_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\officer_spc_multi_latpat_co.paa), QPATHTOF(data\ia_soldier_01_clothing_multi_latpat_co.paa)};
        class ItemInfo : UniformItem {
            uniformModel = "-";
            uniformClass = QEGVAR(soldiers,Officer_Multi_LATPAT);
            containerClass = "Supply40";
            mass = 40;
        };
    };
};

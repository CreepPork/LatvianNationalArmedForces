#define COMPONENT uniforms
#define COMPONENT_BEAUTIFIED Uniforms
#include "\x\lnaf\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE
// #define ENABLE_PERFORMANCE_COUNTERS

#ifdef DEBUG_ENABLED_UNIFORMS
    #define DEBUG_MODE_FULL
#endif

#ifdef DEBUG_SETTINGS_UNIFORMS
    #define DEBUG_SETTINGS DEBUG_SETTINGS_UNIFORMS
#endif

#include "\x\lnaf\addons\main\script_macros.hpp"

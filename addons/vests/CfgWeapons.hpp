class CfgWeapons {
    class VestItem;
    class V_TacVest_blk;
    class V_Press_F;
    class V_PlateCarrier1_blk;
    class V_PlateCarrier2_blk;
    class V_PlateCarrierIAGL_dgtl;
    class V_PlateCarrierIA1_dgtl;
    class V_PlateCarrierIA2_dgtl;
    class V_BandollierB_khk;
    class V_EOD_blue_F;

    class GVAR(TacVest_LATPAT) : V_TacVest_blk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(TacVest_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\tacticalvest_latpat_co.paa)};
    };

    class GVAR(Press_LATPAT) : V_Press_F {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Press_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_press_vest_01_latpat_co.paa)};
    };

    class GVAR(PlateCarrier1_LATPAT) : V_PlateCarrier1_blk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrier1_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\vests_latpat_co.paa)};
    };

    class GVAR(PlateCarrier2_LATPAT) : V_PlateCarrier2_blk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrier2_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\vests_latpat_co.paa)};
    };

    class GVAR(PlateCarrierIAGL_LATPAT) : V_PlateCarrierIAGL_dgtl {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrierIAGL_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_latpat_co.paa), QPATHTOF(data\ga_carrier_gl_rig_latpat_co.paa)};
    };

    class GVAR(PlateCarrierIA1_LATPAT) : V_PlateCarrierIA1_dgtl {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrierIA1_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_latpat_co.paa)};
        hiddenSelections[] = {"camo"};
        class ItemInfo : VestItem {
            containerClass = "Supply120";
            mass = 60;
            uniformModel = "A3\Characters_F_Beta\INDEP\equip_ia_vest01";
            hiddenSelections[] = {"camo"};
            class HitpointsProtectionInfo
            {
                class Chest
                {
                    hitpointName="HitChest";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Diaphragm
                {
                    hitpointName="HitDiaphragm";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Abdomen
                {
                    hitpointName="HitAbdomen";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Body
                {
                    hitpointName="HitBody";
                    passThrough=0.30000001;
                };
            };
        };
    };

    class GVAR(PlateCarrierIA2_LATPAT) : V_PlateCarrierIA2_dgtl {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrierIA2_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_latpat_co.paa)};
        hiddenSelections[] = {"camo"};
        class ItemInfo : VestItem {
            containerClass = "Supply120";
            mass = 80;
            uniformModel = "A3\Characters_F_Beta\INDEP\equip_ia_vest02";
            hiddenSelections[] = {"camo"};
            class HitpointsProtectionInfo
            {
                class Chest
                {
                    hitpointName="HitChest";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Diaphragm
                {
                    hitpointName="HitDiaphragm";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Abdomen
                {
                    hitpointName="HitAbdomen";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Pelvis
                {
                    hitpointName="HitPelvis";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Body
                {
                    hitpointName="HitBody";
                    passThrough=0.30000001;
                };
            };
        };
    };

    class GVAR(BandolierB_LATPAT) : V_BandollierB_khk {
        author = ECSTRING(main,Author);
        displayName = CSTRING(BandolierB_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\vests_latpat_co.paa)};
    };

    class GVAR(EOD_LATPAT) : V_EOD_blue_F {
        author = ECSTRING(main,Author);
        displayName = CSTRING(EOD_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\V_EOD_vest_latpat_CO.paa), QPATHTOF(data\V_EOD_protection_latpat_CO.paa)};
    };

    /**
     * MultiLATPAT
     */
     class GVAR(TacVest_Multi_LATPAT) : GVAR(TacVest_LATPAT) {
         author = ECSTRING(main,Author);
        displayName = CSTRING(TacVest_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\tacticalvest_multi_latpat_co.paa)};
    };

    class GVAR(Press_Multi_LATPAT) : GVAR(Press_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(Press_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_press_vest_01_multi_latpat_co.paa)};
    };

    class GVAR(PlateCarrier1_Multi_LATPAT) : GVAR(PlateCarrier1_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrier1_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\vests_multi_latpat_co.paa)};
    };

    class GVAR(PlateCarrier2_Multi_LATPAT) : GVAR(PlateCarrier2_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrier2_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\vests_multi_latpat_co.paa)};
    };

    class GVAR(PlateCarrierIAGL_Multi_LATPAT) : GVAR(PlateCarrierIAGL_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrierIAGL_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_multi_latpat_co.paa), QPATHTOF(data\ga_carrier_gl_rig_latpat_co.paa)};
    };

    class GVAR(PlateCarrierIA1_Multi_LATPAT) : GVAR(PlateCarrierIA1_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrierIA1_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_multi_latpat_co.paa)};
        hiddenSelections[] = {"camo"};
        class ItemInfo : VestItem {
            containerClass = "Supply120";
            mass = 60;
            uniformModel = "A3\Characters_F_Beta\INDEP\equip_ia_vest01";
            hiddenSelections[] = {"camo"};
            class HitpointsProtectionInfo
            {
                class Chest
                {
                    hitpointName="HitChest";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Diaphragm
                {
                    hitpointName="HitDiaphragm";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Abdomen
                {
                    hitpointName="HitAbdomen";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Body
                {
                    hitpointName="HitBody";
                    passThrough=0.30000001;
                };
            };
        };
    };

    class GVAR(PlateCarrierIA2_Multi_LATPAT) : GVAR(PlateCarrierIA2_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(PlateCarrierIA2_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_multi_latpat_co.paa)};
        hiddenSelections[] = {"camo"};
        class ItemInfo : VestItem {
            containerClass = "Supply120";
            mass = 80;
            uniformModel = "A3\Characters_F_Beta\INDEP\equip_ia_vest02";
            hiddenSelections[] = {"camo"};
            class HitpointsProtectionInfo
            {
                class Chest
                {
                    hitpointName="HitChest";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Diaphragm
                {
                    hitpointName="HitDiaphragm";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Abdomen
                {
                    hitpointName="HitAbdomen";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Pelvis
                {
                    hitpointName="HitPelvis";
                    armor=16;
                    passThrough=0.30000001;
                };
                class Body
                {
                    hitpointName="HitBody";
                    passThrough=0.30000001;
                };
            };
        };
    };

    class GVAR(BandolierB_Multi_LATPAT) : GVAR(BandolierB_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(BandolierB_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\vests_multi_latpat_co.paa)};
    };

    class GVAR(EOD_Multi_LATPAT) : GVAR(EOD_LATPAT) {
        author = ECSTRING(main,Author);
        displayName = CSTRING(EOD_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\V_EOD_vest_multi_latpat_CO.paa), QPATHTOF(data\V_EOD_protection_multi_latpat_CO.paa)};
    };
};

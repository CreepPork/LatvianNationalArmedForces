#define COMPONENT vests
#define COMPONENT_BEAUTIFIED Vests
#include "\x\lnaf\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE
// #define ENABLE_PERFORMANCE_COUNTERS

#ifdef DEBUG_ENABLED_VESTS
    #define DEBUG_MODE_FULL
#endif

#ifdef DEBUG_SETTINGS_VESTS
    #define DEBUG_SETTINGS DEBUG_SETTINGS_VESTS
#endif

#include "\x\lnaf\addons\main\script_macros.hpp"

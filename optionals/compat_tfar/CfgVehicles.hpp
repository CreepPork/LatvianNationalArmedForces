class CfgVehicles {
    class tf_rt1523g;
    class tf_rt1523g_big;

    /**
     * LATPAT
     */
    class GVAR(RT1523_LATPAT) : tf_rt1523g {
        author = ECSTRING(main,Author);
        displayName = CSTRING(RT1523_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\rt1523g_small_latpat_co.paa)};
    };

    class GVAR(RT1523_BIG_LATPAT) : tf_rt1523g_big {
        author = ECSTRING(main,Author);
        displayName = CSTRING(RT1523_BIG_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\rt1523g_big_latpat_co.paa)};
    };

    /**
     * MultiLATPAT
     */
    class GVAR(RT1523_Multi_LATPAT) : GVAR(RT1523_LATPAT) {
        displayName = CSTRING(RT1523_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\rt1523g_small_multi_latpat_co.paa)};
    };

    class GVAR(RT1523_BIG_Multi_LATPAT) : GVAR(RT1523_BIG_LATPAT) {
        displayName = CSTRING(RT1523_BIG_Multi_LATPAT);
        hiddenSelectionsTextures[] = {QPATHTOF(data\rt1523g_big_multi_latpat_co.paa)};
    };
};

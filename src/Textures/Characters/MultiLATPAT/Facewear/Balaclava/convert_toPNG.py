import os
import subprocess
rootdir = os.path.dirname(os.path.realpath(__file__))
extensions = ('.paa')

for subdir, dirs, files in os.walk(rootdir):
	for file in files:
		ext = os.path.splitext(file)[-1].lower()
		if ext in extensions:
			print ("Converting " + file)
			subprocess.call(['E:\Games\SteamApps\common\Arma 3 Tools\TexView2\Pal2PacE.exe', os.path.join(subdir, file), os.path.join(subdir, file[:-3] + "png")])
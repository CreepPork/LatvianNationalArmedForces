#!/usr/bin/env python3
import os
import subprocess
import sys

extensions = ['.png']

root_dir = "../addons"
if os.path.exists("addons"):
    root_dir = "./addons"

if len(sys.argv) == 0:
    raise ValueError(
        'Directory where to convert files was not provided as an argument.')

for subdir, dirs, files in os.walk(root_dir):
    for file in files:
        ext = os.path.splitext(file)[-1].lower()
        if ext in extensions:
            print("Converting " + file)
            subprocess.call(
                ['E:\\Games\\SteamApps\\common\\Arma 3 Tools\\ImageToPAA\\ImageToPAA.exe', os.path.join(subdir, file)])
